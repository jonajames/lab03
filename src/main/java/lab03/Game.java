package lab03;

public class Game {
    private static final int PINS = 10;
    private static final int ROLLS = 21;
    private int roll = 0;
    private int[] rolls = new int[ROLLS];

    public void roll(int pins) {
        if (roll==ROLLS) return;
        rolls[roll] = pins;
        roll++;
        if (pins==PINS && roll%2==1 && roll<ROLLS-3) {
            roll(-1);
        }
    }

    public int score() {
        int score = 0;
        for (int i=0;i<roll;i+=2) {
            int turn = 0;
            if (i<ROLLS-3) {
                turn+=rolls[i]+rolls[i+1];
                if (rolls[i]==PINS) turn = PINS+next2BallScore(i+2);
                else if (turn==PINS) turn += nextBallScore(i+2);
            } else {
                turn = rolls[i];
                i--;
            }
            score+=turn;
        }
        return score;
    }

    private int nextBallScore(int i) {
        for(;i<ROLLS;i++)
            if (rolls[i]>=0)
                return rolls[i];
        return 0;
    }

    private int next2BallScore(int i) {
        int score = -1;
        for(;i<ROLLS;i++)
            if (rolls[i]>=0 && score>=0) {
                return score+rolls[i];
            }
            else if (rolls[i]>=0 && score<0)
                score = rolls[i];
        return (score<0)?0:score;

    }
}
